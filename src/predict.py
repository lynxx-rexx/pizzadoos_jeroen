import tensorflow as tf
from PIL import Image
import numpy as np
from skimage import transform
import requests
from keras.models import load_model
from io import BytesIO
import boto3
import h5py
import contextlib
import json
from datetime import datetime

s3 = boto3.resource('s3')
client = boto3.client('stepfunctions',region_name='eu-central-1')

def load_existing_model(bucket, path):
    obj = s3.Object(bucket, path)
    body = obj.get()['Body'].read()
    file_access_property_list = h5py.h5p.create(h5py.h5p.FILE_ACCESS)
    file_access_property_list.set_fapl_core(backing_store=False)
    file_access_property_list.set_file_image(body)
    file_id_args = {
        'fapl': file_access_property_list,
        'flags': h5py.h5f.ACC_RDONLY,
        'name': b'this should never matter',
    }

    h5_file_args = {
        'backing_store': False,
        'driver': 'core',
        'mode': 'r',
    }

    with contextlib.closing(h5py.h5f.open(**file_id_args)) as file_id:
        with h5py.File(file_id, **h5_file_args) as h5_file:
            loaded_model = load_model(h5_file)  # from keras.models

    return loaded_model

def json_values(obj, key):
    arr = []

    def extract(obj, arr, key):
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    results = extract(obj, arr, key)
    return results


def load_and_resize_img(url):
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))
    np_image = np.array(img).astype('float32')/255
    np_image = transform.resize(np_image, (128, 128, 3))
    np_image = np.expand_dims(np_image, axis=0)
    return np_image

def get_execution_arn():
    stfn = boto3.client('stepfunctions')
    response = stfn.list_executions(
        stateMachineArn='arn:aws:states:eu-central-1:733668268665:stateMachine:StateMachineJeroen',
        statusFilter='RUNNING',
        maxResults=5,
    )
    return response['executions'][-1]['executionArn']

def extract_input_step_function():
    try:
        executionArn = get_execution_arn()
        stfn = boto3.client('stepfunctions')
        response = stfn.describe_execution(
            executionArn=executionArn,
        )
        response_dict = json.loads(response['input'])
        url = response_dict['url']
        name = response_dict['name']
    except:
        raise ValueError('A very specific bad thing happened.')
    return url, name

def process_result(prediction_result):
    if np.argmax(prediction_result) == 1:
        prediction = "Dog"
    else:
        prediction = "Cat"

    accuracy = prediction_result[0][np.argmax(prediction_result)]
    if accuracy > 0.9:
        confidence = "Absolutely certain"
    elif accuracy > 0.8:
        confidence = "Certain"
    elif accuracy > 0.7:
        confidence = "Somewhat certain"
    elif accuracy > 0.6:
        confidence = "Uncertain"
    else:
        confidence = "Very uncertain"

    return prediction, confidence

def run_predict():
    model = load_existing_model('heidag-data-engineer-project', 'models/model4_60epoch.h5')
    activity_url, name = extract_input_step_function()
    img  = load_and_resize_img(activity_url)
    result=model.predict(img, verbose=1)
    prediction, confidence = process_result(result)
    output_dict = {
        "prediction": prediction,
        "confidence": confidence
    }
    json_object = json.dumps(output_dict)
    print(json_object)
    now = datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    s3object = s3.Object('heidag-data-engineer-project', f'pizzadoos_sessie/{name}/{now}.json')
    s3object.put(
        Body=(bytes(json.dumps(json_object).encode('UTF-8')))
    )
    print("Finished script")

run_predict()