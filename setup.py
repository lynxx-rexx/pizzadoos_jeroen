from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Pizzadoos applicatie',
    author='Jeroen Swart',
    license='BSD-3',
)